#ifndef __DRIVER_H_
#define __DRIVER_H_

void SystemClock_Config(void);
void GPIO_Init(void);
void DO_Init(void);
void DO_Write_All(uint8_t *DO_duty);
void DO_Write(int n, uint8_t DO_duty);
void DO_pwm_set_frequency(uint32_t *frequency);
uint8_t DI_Read_All(void);
uint8_t DI_Read(int n);
void AO_Init(void);
void AO_Write_All(float *AO_value);
void AO_Write(int n, float AO_value);
void AI_Init(void);
int AI_Read(int n);
void AI_Read_All(int *AI_value);
void AI18_Set_Gain(uint8_t gain);
void Counter_Init(void);
uint32_t Counter_Read(void);
void Counter_Reset(void);
void Sample_Timer_Init(void);
void Sample_Timer_Set_Period(uint32_t time_ms);

#endif
