#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include "stm32f3xx_hal.h"
#include "driver.h"
#include "usb_device.h"
#include "usbd_custom_hid_if.h"

// Variables
static uint8_t DI_value;
static float AO_value[2];
static int AI_value[3];
static uint32_t C0_value;
static uint32_t DO_pwm_frequency[3];
static uint16_t Ts_ms =100;//ms
static uint8_t usb_rx_buffer[64];
static uint8_t usb_tx_buffer[17];
static volatile uint8_t usb_tx_flag = 0;
static volatile uint8_t usb_rx_flag = 0;

void USB_RX_Interrupt(void);
void Sample_Timer_Interrupt(void);



int main(void)
{
	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
 HAL_Init();
 /* Configure the system clock */
 SystemClock_Config();
 /* Initialize all configured peripherals */
 GPIO_Init();
 AI_Init();
	DO_Init();
 AO_Init();
 Counter_Init();
 MX_USB_DEVICE_Init();
 Sample_Timer_Init();
 while (1)
 {
 if(usb_tx_flag)
 {
 usb_tx_flag =0;
 DI_value = DI_Read_All();
 C0_value = Counter_Read();
 AI_Read_All(AI_value);
 /* DIN */
 usb_tx_buffer[0] = DI_value;
 /* counter */
 usb_tx_buffer[1] = (uint8_t)(C0_value >> 24);
 usb_tx_buffer[2] = (uint8_t)(C0_value >> 16);
 usb_tx_buffer[3] = (uint8_t)(C0_value >> 8);
 usb_tx_buffer[4] = (uint8_t)(C0_value);
 /* AIN */
 usb_tx_buffer[5] = (uint8_t)(AI_value[0] >> 24);
 usb_tx_buffer[6] = (uint8_t)(AI_value[0] >> 16);
 usb_tx_buffer[7] = (uint8_t)(AI_value[0] >> 8);
 usb_tx_buffer[8] = (uint8_t)(AI_value[0]);
 usb_tx_buffer[9] = (uint8_t)(AI_value[1] >> 24);
 usb_tx_buffer[10] = (uint8_t)(AI_value[1] >> 16);
 usb_tx_buffer[11] = (uint8_t)(AI_value[1] >> 8);
 usb_tx_buffer[12] = (uint8_t)(AI_value[1]);
usb_tx_buffer[13] = (uint8_t)(AI_value[2] >> 24);
 usb_tx_buffer[14] = (uint8_t)(AI_value[2] >> 16);
 usb_tx_buffer[15] = (uint8_t)(AI_value[2] >> 8);
 usb_tx_buffer[16] = (uint8_t)(AI_value[2]);
 USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS,usb_tx_buffer,17);
 }
 if (usb_rx_flag)
 {
 int i;
 usb_rx_flag = 0;
 switch (usb_rx_buffer[0])/* cmd id */
 {
 case 'N':/* DO, AO */
 {
 //DO
 DO_Write_All(&usb_rx_buffer[1]);
 //AO
 AO_value[0] = ((float)usb_rx_buffer[9]*256+(float)usb_rx_buffer[10])/1000;
 AO_value[1] = ((float)usb_rx_buffer[11]*256+(float)usb_rx_buffer[12])/1000;
 AO_Write_All(AO_value);
 //reset Counter
 if (usb_rx_buffer[13] == 'R')
 {
 Counter_Reset();
 }
 break;
 }
 case 'F':/* pwm frequency */
 {
 for(i=0;i<3;i++)
 {
 DO_pwm_frequency[i]= (uint32_t)(usb_rx_buffer[i+1]);
 }
 DO_pwm_set_frequency(DO_pwm_frequency);
 break;
 }
 case 'G':/* ADC 18 bit gain */
 {
 AI18_Set_Gain(usb_rx_buffer[1]);
 break;
 }
 case 'T':/* sample time */
 {
 Ts_ms = ((int)usb_rx_buffer[1]<<8) + (int)usb_rx_buffer[2];
 Sample_Timer_Set_Period(Ts_ms);
 }
 }
 }
 }
}

void USB_RX_Interrupt(void)
{
int i;
	USBD_CUSTOM_HID_HandleTypeDef
*myusb=(USBD_CUSTOM_HID_HandleTypeDef *)hUsbDeviceFS.pClassData;
//myusb->Report_buf[0]= numbers of byte data
for (i=0;i<myusb->Report_buf[0];i++)
{
usb_rx_buffer[i]=myusb->Report_buf[i+1];
}
usb_rx_flag = 1;
}

void Sample_Timer_Interrupt(void)
{
usb_tx_flag =1;
}

void _Error_Handler(char *file, int line)
{
 /* USER CODE BEGIN Error_Handler_Debug */
 /* User can add his own implementation to report the HAL error return state */
 while(1)
 {
 }
 /* USER CODE END Error_Handler_Debug */
}
#ifdef USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
 /* USER CODE BEGIN 6 */
 /* User can add his own implementation to report the file name and line number,
 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
 /* USER CODE END 6 */
}
#endif

