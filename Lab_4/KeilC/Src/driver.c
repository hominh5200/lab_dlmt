/******************************************************************************
 * 							        INCLUDE                                    						*
 ******************************************************************************/
#include <math.h>
#include <stdio.h>
#include "stm32f3xx_hal.h"
#include "driver.h"
#include "main.h"
#include "usb_device.h"
#include "usbd_custom_hid_if.h"

/******************************************************************************
 * 							        DEFINE                                    						*
 ******************************************************************************/


/******************************************************************************
 * 							    GLOBAL VARIABLES                               						*
 ******************************************************************************/
 /* HAL variables */
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;
DAC_HandleTypeDef hdac1;
I2C_HandleTypeDef hi2c2;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;
TIM_HandleTypeDef htim15;
TIM_HandleTypeDef htim16;
TIM_HandleTypeDef htim19;
UART_HandleTypeDef huart2;

/******************************************************************************
 * 							   PRIVATE VARIABLES                               						*
 ******************************************************************************/
/* AIN */
static uint16_t ADC_Value[2];
static uint8_t ADC_18_conf[2], ADC_18_rec[3];
static volatile uint8_t ADC_done = 0;

/******************************************************************************
 * 							   EXTERN FUNCTIONS                                						*
 ******************************************************************************/
extern void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
extern void Sample_Timer_Interrupt(void);

/******************************************************************************
 * 							   PRIVATE FUNCTIONS                                					*
 ******************************************************************************/
static void MX_DMA_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM16_Init(void);
static void MX_TIM15_Init(void);
static void MX_ADC1_Init(void);
static void MX_DAC1_Init(void);
static void MX_I2C2_Init(void);
static void MX_TIM19_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM6_Init(void);
 
/* ADC1 init function */
static void MX_ADC1_Init(void)
{
  ADC_ChannelConfTypeDef sConfig;

  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_9;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
}
/* DAC1 init function */
static void MX_DAC1_Init(void)
{
  DAC_ChannelConfTypeDef sConfig;

    /**DAC Initialization 
    */
  hdac1.Instance = DAC1;
  if (HAL_DAC_Init(&hdac1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**DAC channel OUT1 config 
    */
  sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**DAC channel OUT2 config 
    */
  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2C2 init function */
static void MX_I2C2_Init(void)
{
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x0000020B;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Analogue filter 
    */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Digital filter 
    */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 72-1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 100-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim2);
}

/* TIM6 init function */
static void MX_TIM6_Init(void)
{
  TIM_MasterConfigTypeDef sMasterConfig;

  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 36000-1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 99;//50ms
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}
/* TIM15 init function */
static void MX_TIM15_Init(void)
{
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

  htim15.Instance = TIM15;
  htim15.Init.Prescaler = 72-1;
  htim15.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim15.Init.Period = 100-1;
  htim15.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim15.Init.RepetitionCounter = 0;
  htim15.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_PWM_Init(&htim15) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim15, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim15, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_PWM_ConfigChannel(&htim15, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim15, &sBreakDeadTimeConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim15);
}

/* TIM16 init function */
static void MX_TIM16_Init(void)
{
  TIM_OC_InitTypeDef sConfigOC;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

  htim16.Instance = TIM16;
  htim16.Init.Prescaler = 72-1;
  htim16.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim16.Init.Period = 100-1;
  htim16.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim16.Init.RepetitionCounter = 0;
  htim16.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim16) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_PWM_Init(&htim16) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim16, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim16, &sBreakDeadTimeConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim16);

}

/* TIM19 init function */
static void MX_TIM19_Init(void)
{
  TIM_Encoder_InitTypeDef sConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim19.Instance = TIM19;
  htim19.Init.Prescaler = 0;
  htim19.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim19.Init.Period = 65535;
  htim19.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim19.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI1;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim19, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim19, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 19200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
}
/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/******************************************************************************
 * 							   PUBLIC FUNCTIONS                                						*
 ******************************************************************************/
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_I2C2|RCC_PERIPHCLK_ADC1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_HSI;
  PeriphClkInit.USBClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  PeriphClkInit.Adc1ClockSelection = RCC_ADC1PCLK2_DIV2;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

void GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pins : PA0 PA1 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PE8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : PE9 */
  GPIO_InitStruct.Pin = GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PA8 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PF6 PF7 */
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : PB5 PB8 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);	
}

void DO_Init(void)
{
	uint8_t DO_val[8]={0,0,0,0,0,0,0,0};
	uint32_t frequency[3]={2,2,2};//2 KHz
	MX_TIM2_Init();
	MX_TIM16_Init();
	MX_TIM15_Init();
	DO_pwm_set_frequency(frequency);
	DO_Write_All(DO_val);
	HAL_TIM_PWM_Start(&htim15,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim15,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim16,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_4);	
}

void DO_Write_All(uint8_t *DO_duty)
{
	uint32_t DO_pwm_on_time[5];
	int i;
	for (i=0;i<8;i++)
	{
		if (DO_duty[i] > 100)
			DO_duty[i] = 100;
	}
	
	DO_pwm_on_time[0] = DO_duty[0]*((TIM15->ARR)+1)/100;
	DO_pwm_on_time[1] = DO_duty[1]*((TIM15->ARR)+1)/100;
	DO_pwm_on_time[2] = DO_duty[3]*((TIM16->ARR)+1)/100;
	DO_pwm_on_time[3] = DO_duty[6]*((TIM2->ARR)+1)/100;
	DO_pwm_on_time[4] = DO_duty[7]*((TIM2->ARR)+1)/100;
	
	/* DO1 */
	TIM15->CCR1=DO_pwm_on_time[0];
	/* DO2 */
	TIM15->CCR2=DO_pwm_on_time[1];	
	/* DO3 */
	if(DO_duty[2] == 0)
	{
		HAL_GPIO_WritePin(GPIOE,GPIO_PIN_8,GPIO_PIN_RESET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOE,GPIO_PIN_8,GPIO_PIN_SET);
	}
	/* DO4 */
	TIM16->CCR1=DO_pwm_on_time[2];
	/* DO5 */
	if(DO_duty[4] == 0)
	{
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_RESET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_SET);
	}	
	/* DO6 */
	if(DO_duty[5] == 0)
	{
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_RESET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_SET);
	}
	/* DO7 */
	TIM2->CCR4=DO_pwm_on_time[3];	
	/* DO8 */	
	TIM2->CCR3=DO_pwm_on_time[4];	
}	

void DO_Write(int n, uint8_t DO_duty)
{
	if (DO_duty > 100)
		DO_duty = 100;	
	switch (n)
	{
		case 0:
		{
			TIM15->CCR1 = DO_duty * ((TIM15->ARR)+1)/100;			
			break;
		}
		case 1:
		{
			TIM15->CCR2 = DO_duty * ((TIM15->ARR)+1)/100;	
			break;
		}
		case 2:
		{
			if(DO_duty == 0)
			{
				HAL_GPIO_WritePin(GPIOE,GPIO_PIN_8,GPIO_PIN_RESET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOE,GPIO_PIN_8,GPIO_PIN_SET);
			}
			break;
		}
		case 3:
		{
			TIM16->CCR1 = DO_duty * ((TIM16->ARR)+1)/100;	
			break;
		}		
		case 4:
		{
			if(DO_duty == 0)
			{
				HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_RESET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_SET);
			}	
			break;
		}
		case 5:
		{
			if(DO_duty == 0)
			{
				HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_RESET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_SET);
			}			
			break;
		}
		case 6:
		{
			TIM2->CCR4 = DO_duty * ((TIM2->ARR)+1)/100;	
			break;
		}
		case 7:
		{
			TIM2->CCR3 = DO_duty * ((TIM2->ARR)+1)/100;	
			break;
		}
		default:
			break;	
	}
}

void DO_pwm_set_frequency(uint32_t *frequency)//KHz
{
	if (frequency[0])
		TIM15->ARR = 1000/frequency[0]-1;
	if (frequency[1])
		TIM16->ARR = 1000/frequency[1]-1;
	if (frequency[2])
		TIM2->ARR = 1000/frequency[2]-1;	
}

uint8_t DI_Read_All(void)
{
	uint8_t DI_tmp;
	DI_tmp =  HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_8)	  	+   
				(HAL_GPIO_ReadPin(GPIOF,GPIO_PIN_7)<<1)	    + 	
				((!HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_9))<<2)	+		
				(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_5)<<3)	    +		
				(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_15)<<4)    +		
				(HAL_GPIO_ReadPin(GPIOF,GPIO_PIN_6)<<5)	    +	
				(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_8)<<6)	    +		
				(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_9)<<7);
	return DI_tmp;
}

uint8_t DI_Read(int n)
{
	switch (n)
	{
		case 0:
		{
			return HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_8);
		}
		case 1:
		{
			return HAL_GPIO_ReadPin(GPIOF,GPIO_PIN_7);
		}
		case 2:
		{
			return !HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_9);
		}
		case 3:
		{
			return HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_5);
		}
		case 4:
		{
			return HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_15);
		}
		case 5:
		{
			return HAL_GPIO_ReadPin(GPIOF,GPIO_PIN_6);
		}
		case 6:
		{
			return HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_8);
		}
		case 7:
		{
			return HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_9);
		}
		default:
			return 0;
	}
}

void AI_Init(void)
{
	MX_DMA_Init();
	MX_ADC1_Init();/* ADC 12 bit */
	MX_I2C2_Init();/* ADC 18 bit */
	ADC_18_conf[0] = 0x9c;
	HAL_I2C_Master_Transmit(&hi2c2,0xd0,ADC_18_conf,3,100);	
}

int AI_Read(int n)
{
	switch (n)
	{
		case 0:
		{
			ADC_done = 0;
			HAL_ADC_Start_DMA(&hadc1,(uint32_t*)&ADC_Value,2);
			while(!ADC_done);
			HAL_ADC_Stop_DMA(&hadc1);
			return (int)ADC_Value[0];
		}
		case 1:
		{
			ADC_done = 0;
			HAL_ADC_Start_DMA(&hadc1,(uint32_t*)&ADC_Value,2);
			while(!ADC_done);
			HAL_ADC_Stop_DMA(&hadc1);
			return (int)ADC_Value[1];			
		}
		case 2:
		{
			int adc18_tmp;
			HAL_I2C_Master_Transmit(&hi2c2,0xd1,ADC_18_conf,1,100);
			HAL_I2C_Master_Receive(&hi2c2,0xd0,ADC_18_rec,3,100);
			adc18_tmp = ((uint32_t)ADC_18_rec[0]<<16) + ((uint32_t)ADC_18_rec[1]<<8) + (uint32_t)ADC_18_rec[2];
			if (adc18_tmp > 0x3ffff)
				adc18_tmp -= 0x1000000;
			return adc18_tmp;
		}
		default:
			return 0;
	}
}

void AI_Read_All(int *AI_value)
{
	ADC_done = 0;
	HAL_ADC_Start_DMA(&hadc1,(uint32_t*)&ADC_Value,2);
	while(!ADC_done);
	HAL_ADC_Stop_DMA(&hadc1);
	HAL_I2C_Master_Transmit(&hi2c2,0xd1,ADC_18_conf,1,100);
	HAL_I2C_Master_Receive(&hi2c2,0xd0,ADC_18_rec,3,100);	
	AI_value[0] = ADC_Value[0];
	AI_value[1] = ADC_Value[1];
	AI_value[2] = ((uint32_t)ADC_18_rec[0]<<16) + ((uint32_t)ADC_18_rec[1]<<8) + (uint32_t)ADC_18_rec[2];
	if (AI_value[2] > 0x3ffff)
		AI_value[2] -= 0x1000000;	
}

void AI18_Set_Gain(uint8_t gain)
{
	HAL_I2C_Master_Transmit(&hi2c2,0xd0,&gain,3,100);
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	if (hadc->Instance == ADC1)
	{	
		ADC_done = 1;
	}
}

void AO_Init(void)
{
	MX_DAC1_Init();
	AO_Write(0,0);
	AO_Write(1,0);	
	HAL_DAC_Start(&hdac1,DAC_CHANNEL_2);
	HAL_DAC_Start(&hdac1,DAC_CHANNEL_1);	
}

void AO_Write_All(float *AO_value)
{
	AO_Write(0,AO_value[0]);
	AO_Write(1,AO_value[1]);
}

void AO_Write(int n, float AO_value)
{
	uint16_t AO_set=0;
	AO_set = (AO_value/3.3f)*4095;
	switch (n)
	{
		case 0:
			HAL_DAC_SetValue(&hdac1,DAC_CHANNEL_1,DAC_ALIGN_12B_R,(uint32_t) AO_set);	
			break;
		case 1:
			HAL_DAC_SetValue(&hdac1,DAC_CHANNEL_2,DAC_ALIGN_12B_R,(uint32_t) AO_set);
			break;
		default:
			break;
	}
}

void Counter_Init(void)
{
	MX_TIM19_Init();	
	HAL_TIM_Encoder_Start(&htim19, TIM_CHANNEL_1);	
}

uint32_t Counter_Read(void)
{
	uint32_t counter = TIM19->CNT;
	return counter;	
}

void Counter_Reset(void)
{
	TIM19->CNT = 0;	
}

void Sample_Timer_Init(void)
{
  MX_TIM6_Init();
	Sample_Timer_Set_Period(50);
	HAL_TIM_Base_Start_IT(&htim6);	
}

void Sample_Timer_Set_Period(uint32_t Ts_ms)
{
	Ts_ms = Ts_ms*2 -1;
	TIM6->ARR = Ts_ms;	
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
	if (htim->Instance == TIM6)
	{	
		Sample_Timer_Interrupt();
	}
}	
