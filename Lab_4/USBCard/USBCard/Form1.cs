﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibUsbDotNet;
using LibUsbDotNet.Info;
using LibUsbDotNet.Main;

namespace USBCard
{
    public partial class Form1 : Form
    {
        #region Variables
        byte[] DO_value = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
        byte DI_value;

        byte[] AO_0_byte = new byte[2] { 0, 0 };
        byte[] AO_1_byte = new byte[2] { 0, 0 };

        byte[] F_value = new byte[3] { 2, 2, 2 };

        byte[] C_byte = new byte[4];
        int C_value;

        double[] AIValue = new double[3];

        byte resetC = 82; // reset: 82 - 'R', set: 83 - 'S'
        //PID 
        double Ts = 0.01;
        double E=0, E1=0, E2=0;
        double u =0, uk_1=0;
        int setpoint, CurrentPos;
        double Kp = 0, Ki = 0, Kd = 0;
        #endregion Variables

        #region Declaring Global Variable of USB Device
        public static UsbDevice myUsbDevice;
        UsbEndpointReader reader;
        UsbEndpointWriter writer;
        IAsyncResult result;
        bool IsUsbConnected = false;
        #endregion Declaring Global Variable of USB Device

        #region USB_DATA_RECEIVER_INIT
        void USB_DATA_RECEIVER_INIT()
        {
            IUsbDevice wholeUsbDevice = myUsbDevice as IUsbDevice;
            if (!ReferenceEquals(wholeUsbDevice, null))
            {
                wholeUsbDevice.SetConfiguration(1);
                wholeUsbDevice.ClaimInterface(0);
            }
            //Open usb end point reader and writer
            reader = myUsbDevice.OpenEndpointReader(ReadEndpointID.Ep01);
            writer = myUsbDevice.OpenEndpointWriter(WriteEndpointID.Ep01);
            //Set Interrupt service rountie for reader complete event
            reader.DataReceived += (OnRxEndPointData);
            reader.DataReceivedEnabled = true;
        }
        #endregion USB_DATA_RECEIVER_INIT

        #region USB DATA RECEIVER INTERRUPT SERVICE ROUTINE
        Action<byte[]> UsbReceiverAction;
        private void OnRxEndPointData(object sender, EndpointDataEventArgs e)
        {
            UsbReceiverAction = UsbReceiverActionFunction;
            if ((myUsbDevice.IsOpen) && (reader != null))
            {
                result = this.BeginInvoke(UsbReceiverAction, e.Buffer);
            }
        }
        private void UsbReceiverActionFunction(byte[] input)
        {
            DI_value = input[0];
            C_byte[3] = input[1];
            C_byte[2] = input[2];
            C_byte[1] = input[3];
            C_byte[0] = input[4];
            C_value = BitConverter.ToInt32(C_byte, 0);
            if (C_value > 32767)
            {
                C_value = C_value - 65536;
            }
            if (Convert.ToBoolean(DI_value & 0x01))
            {
                ovalDI_0.FillColor = Color.Red;
            }
            else
                ovalDI_0.FillColor = Color.White;
            if (Convert.ToBoolean(DI_value >> 1 & 0x01))
                ovalDI_1.FillColor = Color.Red;
            else
                ovalDI_1.FillColor = Color.White;


            int AI0IntValue;
            int AI1IntValue;
            byte[] AI0ByteValue = new byte[4];
            byte[] AI1ByteValue = new byte[4];

            AI0ByteValue[3] = input[5];
            AI0ByteValue[2] = input[6];
            AI0ByteValue[1] = input[7];
            AI0ByteValue[0] = input[8];
            AI1ByteValue[3] = input[9];
            AI1ByteValue[2] = input[10];
            AI1ByteValue[1] = input[11];
            AI1ByteValue[0] = input[12];

            AI0IntValue = BitConverter.ToInt32(AI0ByteValue, 0);
            AI1IntValue = BitConverter.ToInt32(AI1ByteValue, 0);

            AIValue[0] = (Convert.ToDouble(AI0IntValue) / 4095) * 3.3;
            AIValue[1] = (Convert.ToDouble(AI1IntValue) / 4095) * 3.3;

            //setpoint = int.Parse(txtPos.Text);

            if(chkPID.Checked)
            {
                CurrentPos = C_value;
                E = setpoint - CurrentPos;
                u = uk_1 + Kp * (E - E1) + Ki * (E + E1) / 2 + Kd * (E - 2 * E1 + E2) / Ts;
                uk_1 = u;
                E2 = E1;
                E1 = E;
                byte [] data = { 14, (byte)'N',0, 0, DO_value[2], DO_value[3], DO_value[4], DO_value[5], DO_value[6], DO_value[7], AO_0_byte[0], AO_0_byte[1], AO_1_byte[0], AO_1_byte[1], resetC };
                if( u > 0)
                {
                    data[3] = (byte)u;
                }
                else
                {
                    u = -u;
                    data[2] = (byte)u;
                }
                int bytesWritten;
                writer.Write(data, 1000, out bytesWritten);

            }    

            txtAI0.Text = AIValue[0].ToString("0.000");
            txtAI1.Text = AIValue[1].ToString("0.000");
            txtCounter_0.Text = C_value.ToString();
        }

        #endregion USB DATA RECEIVER INTERRUPT SERVICE ROUTINE

        #region USB DATA SEND DATA
        private void UsbSendData(string command)
        {
            // ham gui du lieu xuong usb card
            if (command == "N")
            {
                byte[] data_send = { 14, (byte)'N', DO_value[0], DO_value[1], DO_value[2], DO_value[3], DO_value[4], DO_value[5], DO_value[6], DO_value[7], AO_0_byte[0], AO_0_byte[1], AO_1_byte[0], AO_1_byte[1], resetC };
                try
                {
                    int bytesWritten;
                    writer.Write(data_send, 1000, out bytesWritten);

                }
                catch (Exception err)
                {
                    MessageBox.Show("Can Not Send Data To USB Device\nDetails: " + err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (command == "F")
            {
                byte[] data_send = { 4, (byte)'F', F_value[0], F_value[1], F_value[2] }; //F
                try
                {
                    int bytesWritten;
                    writer.Write(data_send, 1000, out bytesWritten);

                }
                catch (Exception err)
                {
                    MessageBox.Show("Can Not Send Data To USB Device\nDetails: " + err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #endregion USB DATA SEND DATA

        #region USB EXIT
        private void Usb_exit()
        {
            reader.DataReceivedEnabled = false;
            reader.DataReceived -= (OnRxEndPointData);
            this.EndInvoke(result);
            reader.Dispose();
            writer.Dispose();
            if (myUsbDevice != null)
            {
                if (myUsbDevice.IsOpen)
                {
                    IUsbDevice wholeUsbDevice = myUsbDevice as IUsbDevice;
                    if (!ReferenceEquals(wholeUsbDevice, null))
                    {
                        wholeUsbDevice.ReleaseInterface(0);
                    }
                    myUsbDevice.Close();
                }
                myUsbDevice = null;
                UsbDevice.Exit();
            }
        }
        #endregion USB EXIT

        #region UI EVENTS
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            chkDO_0.Enabled = false;
            chkDO_1.Enabled = false;
            chkPWM_0.Enabled = false;
            chkPWM_1.Enabled = false;
            chkPID.Enabled = false;
            txtDuty_0.Enabled = false;
            txtDuty_1.Enabled = false;
            txtFreq_01.Enabled = false;
            txtPos.Enabled = false;
            btnResetCounter_0.Enabled = false;
            btnSetPos.Enabled = false;

            txtFreq_01.Text = F_value[0].ToString();
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Do you want to exit ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            { }
            else
            {
                e.Cancel = true;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                Usb_exit();
                Application.Exit();
            }
            catch
            { }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (!IsUsbConnected)
            {
                if (myUsbDevice == null)
                {
                    UsbRegDeviceList allDevices = UsbDevice.AllDevices;
                    foreach (UsbRegistry usbRegistry in allDevices)
                    {
                        if (usbRegistry.Pid != 22352) continue;

                        if (usbRegistry.Open(out myUsbDevice))
                        {
                            txtProductName.Text = myUsbDevice.Info.ProductString;
                            txtVendorId.Text =
                            myUsbDevice.Info.Descriptor.VendorID.ToString();
                            txtProductId.Text =
                            myUsbDevice.Info.Descriptor.ProductID.ToString();

                            //MessageBox.Show(txtProductName.Text);

                            txtManufacturer.Text =
                            myUsbDevice.Info.ManufacturerString;
                            USB_DATA_RECEIVER_INIT();
                            btnConnect.Text = "Disconnect";

                            chkDO_0.Enabled = true;
                            chkDO_1.Enabled = true;
                            chkPWM_0.Enabled = true;
                            chkPWM_1.Enabled = true;
                            chkPID.Enabled = true;
                            txtDuty_0.Enabled = true;
                            txtDuty_1.Enabled = true;
                            txtFreq_01.Enabled = true;
                            txtPos.Enabled = true;
                            btnResetCounter_0.Enabled = true;
                            btnSetPos.Enabled = true;


                            // reset everything
                            chkDO_0.Checked = false;
                            chkDO_1.Checked = false;
                            chkPWM_0.Checked = false;
                            chkPWM_1.Checked = false;
                            chkPID.Checked = false;
                            txtDuty_0.Text = "";
                            txtDuty_1.Text = "";
                            txtFreq_01.Text = "2";
                            txtPos.Text = "";

                            DO_value[0] = 0;
                            DO_value[1] = 1;
                            resetC = 82;
                            UsbSendData("N");

                            F_value[0] = 2;
                            UsbSendData("F");

                            IsUsbConnected = true;

                        }
                    }
                }
                if (myUsbDevice == null)
                {
                    MessageBox.Show("Device Not Found !!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                Usb_exit();

                btnConnect.Text = "Connect";
                txtProductName.Text = "";
                txtVendorId.Text = "";
                txtProductId.Text = "";
                txtManufacturer.Text = "";

                chkDO_0.Enabled = false;
                chkDO_1.Enabled = false;
                chkPWM_0.Enabled = false;
                chkPWM_1.Enabled = false;
                txtDuty_0.Enabled = false;
                txtDuty_1.Enabled = false;
                txtFreq_01.Enabled = false;
                btnResetCounter_0.Enabled = false;

                IsUsbConnected = false;


            }
        }

        private void chkDO_0_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDO_0.Checked)
            {
                DO_value[0] = 100;
                chkPWM_0.Enabled = false;
                txtDuty_0.Enabled = false;
            }
            else
            {
                DO_value[0] = 0;
                chkPWM_0.Enabled = true;
                txtDuty_0.Enabled = true;

            }
            UsbSendData("N");
        }

        private void chkDO_1_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDO_1.Checked)
            {
                DO_value[1] = 100;
                chkPWM_1.Enabled = false;
                txtDuty_1.Enabled = false;
            }
            else
            {
                DO_value[1] = 0;
                chkPWM_1.Enabled = true;
                txtDuty_1.Enabled = true;

            }
            UsbSendData("N");
        }

        private void chkPWM_0_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPWM_0.Checked)
            {
                chkDO_0.Enabled = false;
            }
            else
            {
                chkDO_0.Enabled = true;
            }
        }

        private void chkPWM_1_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPWM_1.Checked)
            {
                chkDO_1.Enabled = false;
            }
            else
            {
                chkDO_1.Enabled = true;
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void txtDuty_0_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (chkPWM_0.Checked)
                {
                    try
                    {
                        int temp_Duty = int.Parse(txtDuty_0.Text);
                        if ((temp_Duty < 0) | (temp_Duty > 100))
                        {
                            MessageBox.Show("valid value is from 0 to 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            DO_value[0] = (byte)temp_Duty;
                            UsbSendData("N");
                        }
                    }
                    catch
                    {
                        MessageBox.Show("input is not a number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        private void txtDuty_1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (chkPWM_1.Checked)
                {
                    try
                    {
                        int temp_Duty = int.Parse(txtDuty_1.Text);
                        if ((temp_Duty < 0) | (temp_Duty > 100))
                        {
                            MessageBox.Show("valid value is from 0 to 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            DO_value[1] = (byte)temp_Duty;
                            UsbSendData("N");
                        }
                    }
                    catch
                    {
                        MessageBox.Show("input is not a number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        private void txtFreq_01_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (chkPWM_0.Checked)
                {
                    try
                    {
                        int temp_Freq = int.Parse(txtFreq_01.Text);
                        if ((temp_Freq < 0) | (temp_Freq > 240))
                        {
                            MessageBox.Show("valid value is from 1 to 240", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            F_value[0] = (byte)temp_Freq;
                            UsbSendData("F");
                        }
                    }
                    catch
                    {
                        MessageBox.Show("input is not a number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        private void btnResetCounter_0_Click(object sender, EventArgs e)
        {
            resetC = 82;
            UsbSendData("N");
        }

        #endregion UI EVENTS

        private void txtDuty_0_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtFreq_01_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
