﻿namespace USBCard
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.txtVendorId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProductId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtManufacturer = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDuty_1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chkDO_1 = new System.Windows.Forms.CheckBox();
            this.chkPWM_1 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFreq_01 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDuty_0 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkPWM_0 = new System.Windows.Forms.CheckBox();
            this.chkDO_0 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkPID = new System.Windows.Forms.CheckBox();
            this.btnSetPos = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPos = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.AI0 = new System.Windows.Forms.Label();
            this.txtAI1 = new System.Windows.Forms.TextBox();
            this.txtAI0 = new System.Windows.Forms.TextBox();
            this.btnResetCounter_0 = new System.Windows.Forms.Button();
            this.txtCounter_0 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.ovalDI_1 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.ovalDI_0 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Product Name";
            // 
            // txtProductName
            // 
            this.txtProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductName.Location = new System.Drawing.Point(94, 25);
            this.txtProductName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.ReadOnly = true;
            this.txtProductName.Size = new System.Drawing.Size(150, 19);
            this.txtProductName.TabIndex = 1;
            // 
            // txtVendorId
            // 
            this.txtVendorId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorId.Location = new System.Drawing.Point(94, 50);
            this.txtVendorId.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtVendorId.Name = "txtVendorId";
            this.txtVendorId.ReadOnly = true;
            this.txtVendorId.Size = new System.Drawing.Size(150, 19);
            this.txtVendorId.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Vendor ID";
            // 
            // txtProductId
            // 
            this.txtProductId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductId.Location = new System.Drawing.Point(94, 73);
            this.txtProductId.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProductId.Name = "txtProductId";
            this.txtProductId.ReadOnly = true;
            this.txtProductId.Size = new System.Drawing.Size(150, 19);
            this.txtProductId.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 73);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Product ID";
            // 
            // txtManufacturer
            // 
            this.txtManufacturer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtManufacturer.Location = new System.Drawing.Point(94, 98);
            this.txtManufacturer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtManufacturer.Name = "txtManufacturer";
            this.txtManufacturer.ReadOnly = true;
            this.txtManufacturer.Size = new System.Drawing.Size(150, 19);
            this.txtManufacturer.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 96);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Manufacturer";
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(94, 124);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(81, 30);
            this.btnConnect.TabIndex = 8;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtManufacturer);
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtProductName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtProductId);
            this.groupBox1.Controls.Add(this.txtVendorId);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(9, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(248, 218);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "USB Device Connection";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtDuty_1);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.chkDO_1);
            this.groupBox2.Controls.Add(this.chkPWM_1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtFreq_01);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtDuty_0);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.chkPWM_0);
            this.groupBox2.Controls.Add(this.chkDO_0);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(9, 231);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(522, 93);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(211, 51);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "%";
            // 
            // txtDuty_1
            // 
            this.txtDuty_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDuty_1.Location = new System.Drawing.Point(159, 49);
            this.txtDuty_1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtDuty_1.Name = "txtDuty_1";
            this.txtDuty_1.Size = new System.Drawing.Size(48, 19);
            this.txtDuty_1.TabIndex = 11;
            this.txtDuty_1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDuty_1_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(127, 49);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Duty";
            // 
            // chkDO_1
            // 
            this.chkDO_1.AutoSize = true;
            this.chkDO_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDO_1.Location = new System.Drawing.Point(14, 49);
            this.chkDO_1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkDO_1.Name = "chkDO_1";
            this.chkDO_1.Size = new System.Drawing.Size(49, 17);
            this.chkDO_1.TabIndex = 9;
            this.chkDO_1.Text = "Q0.1";
            this.chkDO_1.UseVisualStyleBackColor = true;
            this.chkDO_1.CheckedChanged += new System.EventHandler(this.chkDO_1_CheckedChanged);
            // 
            // chkPWM_1
            // 
            this.chkPWM_1.AutoSize = true;
            this.chkPWM_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPWM_1.Location = new System.Drawing.Point(64, 49);
            this.chkPWM_1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkPWM_1.Name = "chkPWM_1";
            this.chkPWM_1.Size = new System.Drawing.Size(53, 17);
            this.chkPWM_1.TabIndex = 8;
            this.chkPWM_1.Text = "PWM";
            this.chkPWM_1.UseVisualStyleBackColor = true;
            this.chkPWM_1.CheckedChanged += new System.EventHandler(this.chkPWM_1_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(369, 26);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "kHz";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(211, 26);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "%";
            // 
            // txtFreq_01
            // 
            this.txtFreq_01.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFreq_01.Location = new System.Drawing.Point(318, 25);
            this.txtFreq_01.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtFreq_01.Name = "txtFreq_01";
            this.txtFreq_01.Size = new System.Drawing.Size(48, 19);
            this.txtFreq_01.TabIndex = 5;
            this.txtFreq_01.TextChanged += new System.EventHandler(this.txtFreq_01_TextChanged);
            this.txtFreq_01.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFreq_01_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(239, 26);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Frequency 0,1";
            // 
            // txtDuty_0
            // 
            this.txtDuty_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDuty_0.Location = new System.Drawing.Point(160, 24);
            this.txtDuty_0.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtDuty_0.Name = "txtDuty_0";
            this.txtDuty_0.Size = new System.Drawing.Size(48, 19);
            this.txtDuty_0.TabIndex = 3;
            this.txtDuty_0.TextChanged += new System.EventHandler(this.txtDuty_0_TextChanged);
            this.txtDuty_0.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDuty_0_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(127, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Duty";
            // 
            // chkPWM_0
            // 
            this.chkPWM_0.AutoSize = true;
            this.chkPWM_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPWM_0.Location = new System.Drawing.Point(64, 26);
            this.chkPWM_0.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkPWM_0.Name = "chkPWM_0";
            this.chkPWM_0.Size = new System.Drawing.Size(53, 17);
            this.chkPWM_0.TabIndex = 1;
            this.chkPWM_0.Text = "PWM";
            this.chkPWM_0.UseVisualStyleBackColor = true;
            this.chkPWM_0.CheckedChanged += new System.EventHandler(this.chkPWM_0_CheckedChanged);
            // 
            // chkDO_0
            // 
            this.chkDO_0.AutoSize = true;
            this.chkDO_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDO_0.Location = new System.Drawing.Point(14, 26);
            this.chkDO_0.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkDO_0.Name = "chkDO_0";
            this.chkDO_0.Size = new System.Drawing.Size(49, 17);
            this.chkDO_0.TabIndex = 0;
            this.chkDO_0.Text = "Q0.0";
            this.chkDO_0.UseVisualStyleBackColor = true;
            this.chkDO_0.CheckedChanged += new System.EventHandler(this.chkDO_0_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkPID);
            this.groupBox3.Controls.Add(this.btnSetPos);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.txtPos);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.AI0);
            this.groupBox3.Controls.Add(this.txtAI1);
            this.groupBox3.Controls.Add(this.txtAI0);
            this.groupBox3.Controls.Add(this.btnResetCounter_0);
            this.groupBox3.Controls.Add(this.txtCounter_0);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.shapeContainer1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(262, 10);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(272, 219);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Input";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // chkPID
            // 
            this.chkPID.AutoSize = true;
            this.chkPID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPID.Location = new System.Drawing.Point(162, 164);
            this.chkPID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkPID.Name = "chkPID";
            this.chkPID.Size = new System.Drawing.Size(81, 19);
            this.chkPID.TabIndex = 15;
            this.chkPID.Text = "PID Mode";
            this.chkPID.UseVisualStyleBackColor = true;
            // 
            // btnSetPos
            // 
            this.btnSetPos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetPos.Location = new System.Drawing.Point(173, 130);
            this.btnSetPos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSetPos.Name = "btnSetPos";
            this.btnSetPos.Size = new System.Drawing.Size(33, 24);
            this.btnSetPos.TabIndex = 14;
            this.btnSetPos.Text = "Set";
            this.btnSetPos.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(226, 104);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Position";
            // 
            // txtPos
            // 
            this.txtPos.Location = new System.Drawing.Point(162, 98);
            this.txtPos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPos.Name = "txtPos";
            this.txtPos.Size = new System.Drawing.Size(62, 21);
            this.txtPos.TabIndex = 13;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(226, 68);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(23, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "AI1";
            // 
            // AI0
            // 
            this.AI0.AutoSize = true;
            this.AI0.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AI0.Location = new System.Drawing.Point(226, 25);
            this.AI0.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.AI0.Name = "AI0";
            this.AI0.Size = new System.Drawing.Size(23, 13);
            this.AI0.TabIndex = 1;
            this.AI0.Text = "AI0";
            // 
            // txtAI1
            // 
            this.txtAI1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAI1.Location = new System.Drawing.Point(162, 64);
            this.txtAI1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtAI1.Name = "txtAI1";
            this.txtAI1.Size = new System.Drawing.Size(62, 21);
            this.txtAI1.TabIndex = 7;
            // 
            // txtAI0
            // 
            this.txtAI0.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAI0.Location = new System.Drawing.Point(162, 21);
            this.txtAI0.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtAI0.Name = "txtAI0";
            this.txtAI0.Size = new System.Drawing.Size(62, 21);
            this.txtAI0.TabIndex = 6;
            // 
            // btnResetCounter_0
            // 
            this.btnResetCounter_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetCounter_0.Location = new System.Drawing.Point(65, 124);
            this.btnResetCounter_0.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnResetCounter_0.Name = "btnResetCounter_0";
            this.btnResetCounter_0.Size = new System.Drawing.Size(76, 32);
            this.btnResetCounter_0.TabIndex = 5;
            this.btnResetCounter_0.Text = "Reset C0";
            this.btnResetCounter_0.UseVisualStyleBackColor = true;
            this.btnResetCounter_0.Click += new System.EventHandler(this.btnResetCounter_0_Click);
            // 
            // txtCounter_0
            // 
            this.txtCounter_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCounter_0.Location = new System.Drawing.Point(65, 96);
            this.txtCounter_0.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtCounter_0.Name = "txtCounter_0";
            this.txtCounter_0.ReadOnly = true;
            this.txtCounter_0.Size = new System.Drawing.Size(76, 19);
            this.txtCounter_0.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 100);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Counter 0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(53, 64);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "LED1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(53, 30);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "LED0";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(2, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.ovalDI_1,
            this.ovalDI_0});
            this.shapeContainer1.Size = new System.Drawing.Size(268, 201);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // ovalDI_1
            // 
            this.ovalDI_1.FillColor = System.Drawing.SystemColors.Control;
            this.ovalDI_1.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.ovalDI_1.Location = new System.Drawing.Point(11, 52);
            this.ovalDI_1.Name = "ovalDI_1";
            this.ovalDI_1.Size = new System.Drawing.Size(30, 30);
            // 
            // ovalDI_0
            // 
            this.ovalDI_0.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ovalDI_0.FillColor = System.Drawing.SystemColors.Control;
            this.ovalDI_0.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.ovalDI_0.Location = new System.Drawing.Point(11, 7);
            this.ovalDI_0.Name = "ovalDI_0";
            this.ovalDI_0.Size = new System.Drawing.Size(30, 30);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 371);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Nhom7_V01";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.TextBox txtVendorId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProductId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtManufacturer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkDO_0;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtDuty_0;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkPWM_0;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFreq_01;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovalDI_0;
        private System.Windows.Forms.CheckBox chkDO_1;
        private System.Windows.Forms.CheckBox chkPWM_1;
        private System.Windows.Forms.Label label10;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovalDI_1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDuty_1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnResetCounter_0;
        private System.Windows.Forms.TextBox txtCounter_0;
        private System.Windows.Forms.Label label11;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label AI0;
        private System.Windows.Forms.TextBox txtAI1;
        private System.Windows.Forms.TextBox txtAI0;
        private System.Windows.Forms.CheckBox chkPID;
        private System.Windows.Forms.Button btnSetPos;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPos;
    }
}

